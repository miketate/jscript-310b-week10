class Entry {
    constructor(english, spanish) {
        this.english = english;
        this.spanish = spanish;
    }
}

const entries = [];
entries.push(new Entry("I'm exhausted", "Estoy agotado"));
entries.push(new Entry("I'm exhausted", "Estoy agotado"));
entries.push(new Entry("To talk a lot", "Hablar por los codos"));
entries.push(new Entry("You are annoying", "Eres un pesado"));
entries.push(new Entry("To cross the line", "Pasarse de la raya"));
entries.push(new Entry("That's lame/disappointing", "Que/Menudo chasco te llevarias"));
entries.push(new Entry("To have his hands in everything", "Estar en todos los fregados"));
entries.push(new Entry("Not too bad!", "Ni tan mal"));
entries.push(new Entry("To be unfaithful to someone", "Poner los cuernos"));
entries.push(new Entry("Lovebirds", "Tortolitos"));
entries.push(new Entry("Things were bad and got worse", "Éramos pocos y parió la abuela"));
entries.push(new Entry("To be tipsy", "Estar piripi"));
entries.push(new Entry("I messed it up", "La he pifiado/cagado"));
entries.push(new Entry("A disappointment of a party", "¡Vaya birria de fiesta!"));
entries.push(new Entry("I just embarrassed myself", "¡Trágame, tierra!"));
entries.push(new Entry("It was pure luck", "De chiripa"));
entries.push(new Entry("Don't even bother. Save your breath.", "¡Vete a la porra!"));
entries.push(new Entry("To go out partying", "Salir de marcha"));
entries.push(new Entry("Stop with your crazy ideas", "¡Déjate de chorradas!"));
entries.push(new Entry("No way", "Ni de coña"));
entries.push(new Entry("You're looking like a mess", "Menudo piltrafa estás hecha"));
entries.push(new Entry("Nothing too exciting", "Ni fu ni fa"));
entries.push(new Entry("What a great deal/bargain", "Munudo chollo / menuda ganga"));
entries.push(new Entry("To go out for beers", "Vamos de cañas"));
entries.push(new Entry("They're making us late", "Nos van a dar las uvas"));
entries.push(new Entry("To be really expensive", "Costar un ojo de la cara"));
entries.push(new Entry("To sneak", "Ir a hurtadillas"));
entries.push(new Entry("Don't be annoying", "No seas pelmazo"));
entries.push(new Entry("I have a bad cold", "Menudo trancazo tengo"));
entries.push(new Entry("For crying out loud", "Manda huevos"));
entries.push(new Entry("To really screw it up", "Liarla parda"));
entries.push(new Entry("He's really making it hard on us", "Me trae por la calle de la amargura"));
entries.push(new Entry("To take the blame (even if innocent)", "Comerse el marrón"));
entries.push(new Entry("How boring", "Menudo tostón"));
entries.push(new Entry("Great!  I like it a lot.", "Mola / Me mola mucho"));
entries.push(new Entry("To help out and not just sit there", "Arrimar el hombro"));
entries.push(new Entry("You're losing your mind", "Se te va la olla"));
entries.push(new Entry("To be a tightwad", "Ser un tacaño"));
entries.push(new Entry("To be slow or always late", "Ser un tardón"));
entries.push(new Entry("To be brave, not risk-adverse, confident", "Ser un echado pa'lante"));
entries.push(new Entry("To be a party animal", "Ser un juerguista / fiestero"));
entries.push(new Entry("To not really mean what you say, you are just being nice", "Decirlo con la boca pequeña"));
entries.push(new Entry("Have a good, long weekend", "Feliz puente"));
entries.push(new Entry("To not be able to get along with someone at all", "Tener a alguien entre ceja y ceja"));
entries.push(new Entry("To be stubborn and unlikely to change your mind", "No dar el brazo a torcer"));
entries.push(new Entry("To have half a brain", "Tener dos dedos frente"));
entries.push(new Entry("To think you're the center of the universe", "Creerse el centro/ombligo del mundo"));
entries.push(new Entry("To be sick and tired of something", "Estar hasta las narices"));
entries.push(new Entry("To sleep like a log", "Dormir a pierna suelta"));
entries.push(new Entry("To add insult to injury", "Para más inri"));
entries.push(new Entry("To be very annoying", "Ser mas pesado que una vaca en brazos"));
entries.push(new Entry("I'm under the weather", "Estoy pachucho"));
entries.push(new Entry("He's drunk", "Menudo pedo lleva / Va bolinga"));
entries.push(new Entry("I'm done for the day", "Estoy chof"));
entries.push(new Entry("To go off about something different / change topic", "Irse por las ramas"));
entries.push(new Entry("To worry too much about insignificant stuff", "Ahogarse en un vaso de agua"));
entries.push(new Entry("Simple", "Es pan comido"));
entries.push(new Entry("To get stuck with something undesired", "Bailar con la más fea"));
entries.push(new Entry("The straw that broke the camel's back", "La gota que derramó"));
entries.push(new Entry("To be oblivious to the gravity of the situation", "Hacer castillos en el aire"));
entries.push(new Entry("To be distracted.  Off somewhere else.", "Estar en la luna"));
entries.push(new Entry("To catch red-handed", "Agarrar a alguien con las manos en la masa"));
entries.push(new Entry("To tell someone's dirty secrets", "Sacar los trapitos al sol"));
entries.push(new Entry("To throw in the towel", "Tirar la toalla"));
entries.push(new Entry("To leave behind and start over", "Hacer borrón y cuenta nueva"));
entries.push(new Entry("Be careful.  This is the wrong moment for that.", "El horno no está para bollos"));
entries.push(new Entry("Rob Peter to pay Paul", "Desvistir un santo para vestir otro"));

// ñ ó á ¡ É é