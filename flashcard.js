class PreviousScore {
    constructor(date, correct, asked) {
        this.date = date;
        this.correct = correct;
        this.asked = asked;
    }

    get dateDisplay() {
        return dayjs(this.date).format("M/D h:mm a");
    }
}

const languages = {
    english: "english",
    spanish: "spanish"
}

const gameElements = {
    validationMessage: document.querySelector("#signin-validation"),
    timer: document.querySelector("#timer"),
    currentScore: document.querySelector("#current-score"),
    questionsAsked: document.querySelector("#questions-asked"),
    finalScore: document.querySelector("#final-score"),
    previousResults: document.querySelector("#previous-results"),
    answerCard: document.querySelector("#answer-card"),
    previousScoresTable: document.querySelector("#previous-results"),
    optionCards: document.querySelectorAll("div.selection"),
    playerDisplayName: document.querySelector("#player-name"),
    noPreviousScores: document.querySelector("#no-previous-results"),
    sections: {
        game: document.querySelector("#section-game"),
        signIn: document.querySelector("#section-signin"),
        gameOver: document.querySelector("#section-game-over")
    },
    buttons: {
        playAgain: document.querySelector("#play-again"),
        signOut: document.querySelector("#sign-out")
    },
    inputs: {
        playerName: document.querySelector("#input-name"),
        showAnswerIn: document.querySelector("#input-language")
    }
}

let currentPlayerName = "";
let showAnswerIn = "";
let currentRound = 0;
let currentCorrect = 0;
let currentWrong = 0;
let questionsAsked = 0;

let gameRound = {
    currentAnswerIndex: 0,
    displayedOptions: []
};

function setupNewQuestions() {
    currentRound++;
    gameRound.displayedOptions = [];

    getRandomAnswer();
    getAnotherOption();
    getAnotherOption();
    shuffleArray(gameRound.displayedOptions);
    displayBoard();
    displayPreviousResults();
}

function startTimer() {
    let currentTimer = 30;
    gameElements.timer.innerText = "0:" + currentTimer;
    gameElements.timer.style.borderColor = "green";

    setInterval(() => {
        if (currentTimer > 0) {
            currentTimer--;
            if (currentTimer < 6) {
                timer.style.borderColor = "red";
            }
            else if (currentTimer < 10) {
                timer.style.borderColor = "darkorange";
            }
            else {
                timer.style.borderColor = "green";
            }

            gameElements.timer.innerText = "0:" + currentTimer.toString().leftPad("0", 2);

            if (currentTimer === 0) {
                // Maybe no time to answer the final question, so don't penalize them.
                questionsAsked--;

                hide(gameElements.sections.game);
                show(gameElements.sections.gameOver);
                gameElements.finalScore.innerText = currentCorrect + " out of " + questionsAsked;
                
                if (questionsAsked === 0) { // If they only saw one question but didn't try, then don't record it.
                    return;
                }

                const scoreList = getPreviousScores(currentPlayerName);

                scoreList.unshift(new PreviousScore(new Date(), currentCorrect, questionsAsked));

                localStorage.setItem(currentPlayerName, JSON.stringify(scoreList));                    
            }
        }
    }, 1000);
}

function getPreviousScores(playerName) {
    const previousScoresItem = localStorage.getItem(playerName);
    let scoreList = null;

    if (!previousScoresItem || previousScoresItem.length === 0) {
        scoreList = [];
    }
    else {
        scoreList = JSON.parse(previousScoresItem);
    }
    return scoreList;
}

function displayPreviousResults() {
    const previousScores = getPreviousScores(currentPlayerName);
    if (previousScores.length === 0) {
        show(gameElements.noPreviousScores);
    }
    else {
        hide(gameElements.noPreviousScores);
        let previousScoresTable = "";
        for (let i = 0; i < previousScores.length; i++) {
            let score = previousScores[i];
            // Use this so we can make use of the dateDisplay getter from the prototype
            Object.setPrototypeOf(score, PreviousScore.prototype);
            previousScoresTable += `<tr><td>${score.correct} / ${score.asked}</td><td>${score.dateDisplay}</td></tr>`;
        }
        gameElements.previousScoresTable.innerHTML = previousScoresTable;
    };
}

function getRandomAnswer() {
    const randomIndex = Math.floor(Math.random() * entries.length);
    gameRound.currentAnswerIndex = randomIndex;
    gameRound.displayedOptions.push(randomIndex);
}

function getAnotherOption() {
    const randomIndex = Math.floor(Math.random() * entries.length);
    if (gameRound.displayedOptions.indexOf(randomIndex) > -1) {
        return getAnotherOption();
    }
    gameRound.displayedOptions.push(randomIndex);
}

function shuffleArray(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
}

function displayBoard() {
    gameElements.currentScore.innerText = currentCorrect;
    gameElements.questionsAsked.innerText = questionsAsked;

    for (let i = 0; i < gameRound.displayedOptions.length; i++) {
        let cardData = entries[gameRound.displayedOptions[i]];
        let selector = "#card-" + (i + 1) + " .card-text";
        let cardText = document.querySelector(selector);
        cardText.innerText = showAnswerIn === languages.english ? cardData.spanish : cardData.english;
        let card = cardText.closest(".card");
        card.dataset.index = gameRound.displayedOptions[i];
    }
    let answerCard = entries[gameRound.currentAnswerIndex];
    let cardTextArea = document.querySelector("#answer-card .card-text");
    cardTextArea.innerText = showAnswerIn === languages.english ? answerCard.english : answerCard.spanish;
    cardTextArea.parentElement.dataset.index = gameRound.currentAnswerIndex;
    questionsAsked++;
}

function handleSelect(selectedCard) {
    if (selectedCard.dataset.index * 1 === gameRound.currentAnswerIndex) {
        handleCorrectScore();
    }
    else {
        handleIncorrectScore(selectedCard);
    }
    gameElements.currentScore.innerText = currentCorrect;
    gameElements.questionsAsked.innerText = questionsAsked;
}

function handleCorrectScore() {
    currentCorrect++;
    document.querySelector("div.selection[data-index='" + gameRound.currentAnswerIndex + "']>div").style.backgroundColor = "green";
    setTimeout(() => {
        document.querySelectorAll("div.selection>div").forEach(card => { card.style.backgroundColor = ""; });
        setupNewQuestions();
    }, 1000);
}

function handleIncorrectScore(wrongCard) {
    currentWrong++;
    wrongCard.closest(".card").firstElementChild.style.backgroundColor = "red";
    document.querySelector("div.selection[data-index='" + gameRound.currentAnswerIndex + "']>div").style.backgroundColor = "green";
    setTimeout(() => {
        document.querySelectorAll("div.selection>div").forEach(card => { card.style.backgroundColor = ""; });
        setupNewQuestions();
    }, 3000);        
}

function displayWeather(data) {
    document.querySelector("#weather-temp").innerText = data.temp;
    document.querySelector("#weather-description").innerText = data.description;
    document.querySelector("#weather-icon").src = `http://openweathermap.org/img/wn/${data.icon}@2x.png`;
}

function signOut() {
    hide(gameElements.sections.game);
    hide(gameElements.sections.gameOver);
    show(gameElements.sections.signIn);
    currentPlayerName = "";        
}

function startGame() {
    currentRound = 0;
    currentCorrect = 0;
    currentWrong = 0;
    questionsAsked = 0;

    gameRound = {
        currentAnswerIndex: 0,
        displayedOptions: []
    };

    show(gameElements.sections.game);
    hide(gameElements.sections.gameOver);
    hide(gameElements.sections.signIn);
    gameElements.playerDisplayName.innerText = currentPlayerName;
    startTimer();
    setupNewQuestions();
}

if (currentPlayerName.length > 0) {
    hide(gameElements.sections.signIn);
    show(gameElements.sections.game);
    startGame();
}
else {
    show(gameElements.sections.signIn);
    hide(gameElements.sections.game);
}


getWeatherByCity("Madrid", displayWeather);

/************************
 * Event Handlers
 ***********************/

document.querySelector("#form").addEventListener("submit", function(e) {
    e.preventDefault();
    let validationMessages = [];
    currentPlayerName = gameElements.inputs.playerName.value;
    showAnswerIn = gameElements.inputs.showAnswerIn.value;
    if (currentPlayerName.length === 0) {
        validationMessages.push("Please enter a name to start the game.");
    }
    if (showAnswerIn.length === 0) {
        validationMessages.push("Please select an answer language.");
    }
    if (validationMessages.length === 0) {
        hide(gameElements.validationMessage);
        startGame();
    }
    else {
        showValidationMessages(validationMessages);
        show(gameElements.validationMessage);
    }
});

function showValidationMessages(messages) {
    if (messages.length === 0)
        return;
    
    const list = gameElements.validationMessage.firstElementChild; // Should be the <ul>
    list.innerHTML = ""; // Clear any previous list items.
    for (let i = 0; i < messages.length; i++) {
        let messageItem = document.createElement("li");
        messageItem.innerText = messages[i];
        list.appendChild(messageItem);
    }
}

gameElements.buttons.playAgain.addEventListener("click", startGame);

gameElements.buttons.signOut.addEventListener("click", signOut);

gameElements.optionCards.forEach(card => {
    card.addEventListener("mouseover", e => {
        e.target.closest(".card").classList.add("shadow");
    });

    card.addEventListener("mouseout", e => {
        e.target.closest(".card").classList.remove("shadow");
    });

    card.addEventListener("click", e => {
        handleSelect(e.target.closest(".card"));
    });
});

document.querySelector("#input-city").addEventListener("change", e => {
    const city = e.target.value;
    getWeatherByCity(city, displayWeather);
});

/************************
 * Utility functions
 ***********************/
function hide(element){
    element.style.display = "none";
}

function show(element){
    element.style.display = "block";
}  

String.prototype.leftPad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

function getWeatherByCity(city, callback) {
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=imperial&appid=${API_KEY}`;
    fetch(url)
        .then(response => response.json())
        .then(data => {
            callback({
                temp: data.main.temp + "°",
                description: data.weather[0].main,
                icon: data.weather[0].icon
            });            
        });
}